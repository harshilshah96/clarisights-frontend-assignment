export type ILHS = 'account' | 'country' | 'campaign_name' | 'revenue';

export type IAccountCountryOperators = 'contains' | 'not_contains';
export type ICampaignNameOperators = IAccountCountryOperators | 'starts_with';
export type IRevenueOperators = '>' | '<' | '>=' | '<=' | '=' | '!=';
export type IOperator =
  | IAccountCountryOperators
  | ICampaignNameOperators
  | IRevenueOperators;

export type IAccountRHS = number[];
export type ICountryRHS = string[];
export type ICampaignNameRHS = string;
export type IRevenueRHS = string;
export type IRHS = IAccountRHS | ICountryRHS | ICampaignNameRHS | IRevenueRHS;

export interface IFilter {
  lhs: ILHS | null;
  operator: IOperator | null;
  rhs: IRHS | null;
}

export interface Payload {
  filters: IFilter[];
}

export interface IAction {
  type: string;
  payload: IFilterWithIndex;
}

export type IFilterWithIndex = IFilter & { index: number };

export interface IDropdown {
    label: string;
}

export interface ILHSDropdown extends IDropdown {
    value: ILHS;
}

export interface IAccountCountryOperatorDropdown extends IDropdown {
    value: IAccountCountryOperators;
}

export interface ICampaignNameOperatorDropdown extends IDropdown {
    value: ICampaignNameOperators;
}

export interface IRevenueOperatorDropdown extends IDropdown {
    value: IRevenueOperators;
}

export interface ICountryData {
    name: string;
    code: string;
}
