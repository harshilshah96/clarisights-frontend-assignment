import { ADD_FILTER, REMOVE_FILTER, UPDATE_FILTER } from '../constants/action-types';
import { IAction, IFilterWithIndex } from '../types';

/**
 * Action creators for CRUD operations on the filters
 */
export const addFilter = (filter: IFilterWithIndex): IAction => ({
  type: ADD_FILTER,
  payload: filter,
});

export const removeFilter = (filter: IFilterWithIndex): IAction => ({
  type: REMOVE_FILTER,
  payload: filter,
});

export const updateFilter = (filter: IFilterWithIndex): IAction => ({
  type: UPDATE_FILTER,
  payload: filter,
});
