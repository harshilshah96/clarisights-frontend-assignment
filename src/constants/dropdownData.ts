import {
  account,
  country,
  campaign_name,
  revenue,
  contains,
  not_contains,
  starts_with,
} from './filterConstants';
import {
  ILHSDropdown,
  IAccountCountryOperatorDropdown,
  ICampaignNameOperatorDropdown,
  IRevenueOperatorDropdown,
} from '../types';

export const LHSDropdownData: ILHSDropdown[] = [
  { label: 'Account', value: account },
  { label: 'Country', value: country },
  { label: 'Campaign Name', value: campaign_name },
  { label: 'Revenue', value: revenue },
];

export const AccountCountryOperatorDropdown: IAccountCountryOperatorDropdown[] = [
  { label: 'Contains', value: contains },
  { label: 'Not contains', value: not_contains },
];

export const CampaignNameOperatorDropdown: ICampaignNameOperatorDropdown[] = [
  { label: 'Starts with', value: starts_with },
  { label: 'Contains', value: contains },
  { label: 'Not contains', value: not_contains },
];

export const RevenueOperatorDropdown: IRevenueOperatorDropdown[] = [
  { label: '>', value: '>' },
  { label: '<', value: '<' },
  { label: '>=', value: '>=' },
  { label: '<=', value: '<=' },
  { label: '=', value: '=' },
  { label: '!=', value: '!=' },
];
