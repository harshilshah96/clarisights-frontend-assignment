export const account = 'account';
export const country = 'country';
export const campaign_name = 'campaign_name';
export const revenue = 'revenue';
export const contains = 'contains';
export const not_contains = 'not_contains';
export const starts_with = 'starts_with';
