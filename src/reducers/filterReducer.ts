import { values } from 'lodash';
import {
  ADD_FILTER,
  REMOVE_FILTER,
  UPDATE_FILTER,
} from '../constants/action-types';
import { IAction, IFilterWithIndex, IFilter } from '../types';

/**
 * 
 * The reducer takes in the current state and an action with a payload of the filter to be added, deleted or updated
 * 
 * @param state The current state of the mini reducer
 * @param action The action consists of a type and a payload with the values to be updated in the store
 */
export const filterReducer = (state: IFilter[], action: IAction) => {
  switch (action.type) {
    case ADD_FILTER:
    /**
     * returns a new state with an additional filter row with  empty values
     */
      return [...state, action.payload];
    case REMOVE_FILTER:
    /**
     * returns a new state with the filter row passed through the action payload filtered out
     */
      return [
        ...state.filter(
          (_currentFilter, index) => index !== action.payload.index,
        ),
      ];
    case UPDATE_FILTER:
    /**
     * Updating the current filter row being edited with updated lhs, operator and rhs values.
     * Using the comprehensive method of converting the array to an indexed object, applying changes
     * and returning a converted back array for performance purposes.
     */
      const { payload } = action;
      let stateObject = { ...state };
      stateObject[payload.index] = payload;
      return values(stateObject) as IFilterWithIndex[];
    default:
      return state;
  }
};
