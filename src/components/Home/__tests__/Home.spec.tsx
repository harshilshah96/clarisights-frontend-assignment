import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { shallow } from 'enzyme';
import * as React from 'react';
import Home from '..';
import { countryData } from '../../../services/__mocks__/countryServices';
import { Filter } from '../Filter';
import Select from 'react-select';

jest.mock('../../../services/countryServices.ts');

describe('<Home />', () => {
  it('Adds Filter Row on Add button click', async () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://restcountries.eu/rest/v2/all').reply(200, countryData);
    const component = shallow(<Home />);
    component.find('.add-button').simulate('click');

    /**
     * On clicking the add button a check to see if there are 2 Filter components rendered
     */
    expect(component.find(Filter).length).toEqual(2);
  });

  it('Removes Filter Row on Remove button click', () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://restcountries.eu/rest/v2/all').reply(200, countryData);
    const component = shallow(<Home />);
    component.find('.add-button').simulate('click');
    component
      .find(Filter)
      .first()
      .dive()
      .find('.remove-button')
      .simulate('click');
    expect(component.find(Filter).length).toEqual(1);
  });

  it('Renders Text box on Campaign select', () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://restcountries.eu/rest/v2/all').reply(200, countryData);
    const component = shallow(<Home />);
    const firstFilter = component.find(Filter).dive();
    firstFilter
      .find(Select)
      .first()
      .dive()
      .dive()
      .simulate('mouseDown', { button: 0 });
    expect(firstFilter.find('.campaign-input')).toBeTruthy();
  });

  it('Renders Numeric box on Revenue select', () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://restcountries.eu/rest/v2/all').reply(200, countryData);
    const component = shallow(<Home />);
    const firstFilter = component.find(Filter).dive();
    firstFilter
      .find(Select)
      .first()
      .dive()
      .dive()
      .simulate('mouseDown', { button: 0 });
    expect(firstFilter.find('.revenue-input')).toBeTruthy();
  });

  it('Renders Account filter select dropdown on Account select', () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://restcountries.eu/rest/v2/all').reply(200, countryData);
    const component = shallow(<Home />);
    const firstFilter = component.find(Filter).dive();
    firstFilter
      .find(Select)
      .first()
      .dive()
      .dive()
      .simulate('mouseDown', { button: 0 });
    expect(firstFilter.find('.filter-select-account')).toBeTruthy();
  });

  it('Renders Country filter select dropdown on Country select', () => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://restcountries.eu/rest/v2/all').reply(200, countryData);
    const component = shallow(<Home />);
    const firstFilter = component.find(Filter).dive();
    firstFilter
      .find(Select)
      .first()
      .dive()
      .dive()
      .simulate('mouseDown', { button: 0 });
    expect(firstFilter.find('.filter-select-country')).toBeTruthy();
  });
});
