import * as React from 'react';
import { Button } from 'react-bootstrap';
import { addFilter } from '../../actions/filterActions';
import { filterReducer } from '../../reducers/filterReducer';
import { getAllCountries } from '../../services/countryServices';
import { IFilter } from '../../types';
import { Filter } from './Filter';
import Axios from 'axios';
import './style.scss';

export const initialFilterState: IFilter = {
  lhs: null,
  operator: null,
  rhs: null,
};

export const initialState = [initialFilterState];

const Home = () => {

  /**
   * Since there is currently only one component with a single level of nesting being used,
   * we will use the useReducer hook as a mini Redux store to control state updates
   */
  const [currentFilters, dispatch] = React.useReducer(
    filterReducer,
    initialState,
  );
  /**
   * To set country data through an API
   */
  const [countryData, setCountryData] = React.useState([]);

  React.useEffect(() => {
    Axios.get('https://restcountries.eu/rest/v2/all').then(({ data }) => {
      setCountryData(
        data.map(countryData => {
          return {
            name: countryData.name,
            code: countryData.callingCodes[0],
          };
        }),
      );
    });
    /**
     * Passing empty array to let the useEffect Hook run only once (componentDidMount)
     */
  }, []);

  const renderFilters = () =>
    currentFilters.map((currentFilter, index) => (
      <Filter
        currentFilterLength={currentFilters.length}
        key={`filter-${index}`}
        filter={currentFilter}
        countriesData={countryData}
        dispatch={dispatch}
        /**
         * Passing the rendering index to map the filter number to the index
         */
        index={index}
      />
    ));

  return (
    <div className="home-page-container">
      <h1>Filters:</h1>
      {renderFilters()}
      <div
        className="add-button"
        onClick={() =>
          /**
           * The addFilter action creater renders an additional filter row with empty values
           */
          dispatch(
            addFilter({ ...initialFilterState, index: currentFilters.length }),
          )
        }
      >
        +
      </div>
      <Button
        className="log-to-console-button"
        variant="primary"
        onClick={() => console.log({ filters: currentFilters })}
      >
        Apply
      </Button>
    </div>
  );
};

export default Home;
