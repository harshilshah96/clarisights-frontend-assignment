import * as React from 'react';
import Select from 'react-select';
import { updateFilter, removeFilter } from '../../../actions/filterActions';
import {
  AccountCountryOperatorDropdown,
  CampaignNameOperatorDropdown,
  LHSDropdownData,
  RevenueOperatorDropdown,
} from '../../../constants/dropdownData';
import './style.scss';
import { IAction, ICountryData, ILHSDropdown, IFilter } from '../../../types';

export interface IFilterProps {
  filter: IFilter;
  dispatch: React.Dispatch<IAction>;
  countriesData: ICountryData[];
  index: number;
  currentFilterLength: number;
}

export const Filter = ({
  filter,
  dispatch,
  countriesData,
  index,
  currentFilterLength,
}: IFilterProps) => {
  const handleLHSDropdownSelect = (selectedLHS: ILHSDropdown) =>
    dispatch(
      updateFilter({
        lhs: selectedLHS.value,
        operator: null,
        rhs: null,
        index,
      }),
    );

  const handleOperatorDropdownSelect = selectedOperator =>
    dispatch(
      updateFilter({
        ...filter,
        operator: selectedOperator.value,
        rhs: null,
        index,
      }),
    );

  const operatorDropdownMappings: { [key: string]: any } = {
    account: AccountCountryOperatorDropdown,
    country: AccountCountryOperatorDropdown,
    campaign_name: CampaignNameOperatorDropdown,
    revenue: RevenueOperatorDropdown,
  };

  const getOperatorDropdownOptions = () => operatorDropdownMappings[filter.lhs];

  const handleAccountRHSDropdownChange = (
    selectedArray: Array<{ label: number; value: number }>,
  ) =>
    dispatch(
      updateFilter({
        ...filter,
        rhs: (selectedArray || []).map(selectedNumber => selectedNumber.value),
        index,
      }),
    );

  const handleCountryRHSDropdownChange = (
    selectedArray: Array<{ label: string; value: string }>,
  ) =>
    dispatch(
      updateFilter({
        ...filter,
        index,
        rhs: (selectedArray || []).map(country => country.value),
      }),
    );

  const handleCampaignInputChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    dispatch(updateFilter({ ...filter, rhs: e.currentTarget.value, index }));

  const handleRevenueInputChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    dispatch(updateFilter({ ...filter, rhs: e.currentTarget.value, index }));

  const renderLHSDropdown = () => {
    const lhsDropdownValue =
      filter.lhs &&
      LHSDropdownData.find(dropdownData => dropdownData.value === filter.lhs);
    return (
      <Select
        classNamePrefix="filter-select-lhs"
        value={lhsDropdownValue}
        className="filter-select"
        placeholder="Select Filter Type"
        options={LHSDropdownData}
        onChange={handleLHSDropdownSelect}
      />
    );
  };
  const renderOperatorDropdown = () => {
    const currentOperatorValue =
      filter.operator &&
      [...CampaignNameOperatorDropdown, ...RevenueOperatorDropdown].find(
        dropdownData => dropdownData.value === filter.operator,
      );
    return (
      <Select
        value={currentOperatorValue}
        className="filter-select"
        placeholder="Select operation"
        isDisabled={!filter.lhs}
        options={getOperatorDropdownOptions()}
        onChange={handleOperatorDropdownSelect}
      />
    );
  };

  const renderAccountRHSDropdown = () => {
    let optionsArray = [];
    for (let i = 0; i < 1000; i++) {
      optionsArray.push({ label: i, value: i });
    }
    const selectedValue =
      filter.rhs &&
      (filter.rhs as string[]).map &&
      (filter.rhs as string[]).map(val => ({ label: val, value: val }));
    return (
      <Select
        value={selectedValue}
        className="filter-select filter-select-account"
        placeholder="Enter Values"
        isMulti
        options={optionsArray}
        onChange={handleAccountRHSDropdownChange}
      />
    );
  };

  const renderCountriesRHSDropdown = () => {
    const constructCountriesDropdown = countriesData.map(countryData => ({
      label: countryData.name,
      value: countryData.code,
    }));
    const currentCountryDropdownValues =
      filter.rhs &&
      (filter.rhs as string[]).map &&
      constructCountriesDropdown.filter(currentDropdown =>
        (filter.rhs as string[]).some(
          filterString => filterString === currentDropdown.value,
        ),
      );
    return (
      <Select
        value={currentCountryDropdownValues}
        isMulti
        className="filter-select filter-select-country"
        placeholder={
          countriesData.length ? 'Select countries' : 'Loading countries data'
        }
        options={constructCountriesDropdown}
        onChange={handleCountryRHSDropdownChange}
      />
    );
  };

  const renderCampaignNameRHS = () => (
    <input
      className="campaign-input"
      placeholder="Enter campaign name"
      value={(filter.rhs || '').toString()}
      onChange={handleCampaignInputChange}
    />
  );

  const renderRevenueRHS = () => (
    <input
      className="revenue-input"
      placeholder="Enter Revenue"
      type="number"
      step="0.01"
      value={typeof filter.rhs === 'string' ? filter.rhs : ''}
      onChange={handleRevenueInputChange}
    />
  );

  const rhsMappings = () => ({
    account: renderAccountRHSDropdown(),
    country: renderCountriesRHSDropdown(),
    campaign_name: renderCampaignNameRHS(),
    revenue: renderRevenueRHS(),
  });

  return (
    <div key={`filter-${index}`} className="filter-container">
      {renderLHSDropdown()}
      {renderOperatorDropdown()}
      <div className="rhs-container">
        {filter.operator ? rhsMappings()[filter.lhs] : ''}
      </div>
      {currentFilterLength !== 1 ? (
        <div
          className="remove-button"
          onClick={() => dispatch(removeFilter({ ...filter, index }))}
        >
          X
        </div>
      ) : (
        ''
      )}
    </div>
  );
};
