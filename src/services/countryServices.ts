import { get } from '../utilities/http';

export const getAllCountries = async () => {
  const { data } = await get<any[]>('https://restcountries.eu/rest/v2/all');
  return data.map(countryData => ({
    name: countryData.name,
    code: countryData.callingCodes[0],
  }));  
};
