export const getAllCountries = () => {
  const countryData = [
    { name: 'India', callingCodes: ['91'] },
    { name: 'USA', callingCodes: ['01'] },
    { name: 'Canada', callingCodes: ['02'] },
  ];
  return Promise.resolve(countryData);
};

export const countryData = [
    { name: 'India', callingCodes: '91' },
    { name: 'USA', callingCodes: '01' },
    { name: 'Canada', callingCodes: '02' },
  ];