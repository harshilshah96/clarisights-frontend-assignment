# Clarisights Frontend Assignment

## Before you run: 
- You need to have `npm` installed. 
- You need to have `typescript` installed globally using `npm i -g typescript`

## Steps to run the repo locally:

 - Clone the repo
 - Open the terminal and run `npm i`
 - Run the dev server using `npm start`

## For Production build

- Run `npm run build` in the terminal window after cloning the repo

## For running tests

- Run `npm test` in the terminal window